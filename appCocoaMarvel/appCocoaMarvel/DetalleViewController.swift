//
//  DetalleViewController.swift
//  appCocoaMarvel
//
//  Created by Laboratorio FIS on 3/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class DetalleViewController: UIViewController {
    var pokemon:Pokemon?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title=pokemon?.name
    }

}
