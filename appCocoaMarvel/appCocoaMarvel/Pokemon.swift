//
//  Pokemon.swift
//  appCocoaMarvel
//
//  Created by Laboratorio FIS on 29/11/17.
//  Copyright © 2017 Laboratorio FIS. All rights reserved.
//

import Foundation
import ObjectMapper
class Pokemon: Mappable {
    var id: Int?
    var name: String?
    var height: Double?
    var weight: Double?

required init?(map: Map){
    
}

func mapping(map: Map){
    id <- map["id"]
    name <- map["name"]
    height <- map["height"]
    weight <- map["weight"]
    
}
    
}
