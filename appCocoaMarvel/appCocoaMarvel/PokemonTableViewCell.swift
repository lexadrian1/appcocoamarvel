//
//  PokemonTableViewCell.swift
//  appCocoaMarvel
//
//  Created by Laboratorio FIS on 3/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func fillData(pokemon:Pokemon){
        nameLabel.text = pokemon.name
        heightLabel.text = "\(pokemon.height ?? 0)"
        weightLabel.text = "\(pokemon.weight ?? 0)"
    }

}
