//
//  ViewController.swift
//  appCocoaMarvel
//
//  Created by Laboratorio FIS on 28/11/17.
//  Copyright © 2017 Laboratorio FIS. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descripcionTextView: UITextView!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    @IBAction func consultarButton(_ sender: Any) {
        let pkId = arc4random_uniform(6)+1
        let URL = "https://pokeapi.co/api/v2/pokemon/\(pkId)"
        Alamofire.request(URL).responseObject { (response: DataResponse<Pokemon>) in
            
            let pokemon = response.result.value
            
            DispatchQueue.main.async {
                self.nameLabel.text=pokemon?.name ?? ""
                self.heightLabel.text="\(pokemon?.height ?? 0)"
                self.weightLabel.text="\(pokemon?.weight ?? 0)"
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

