//
//  PokemonViewController.swift
//  appCocoaMarvel
//
//  Created by Laboratorio FIS on 29/11/17.
//  Copyright © 2017 Laboratorio FIS. All rights reserved.
//

import UIKit


class PokemonViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate{
    @IBOutlet weak var pokedexTableView: UITableView!
    var pokemonArray:[Pokemon] = []
    var pokemonIndex = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let service = pokemonService()
        service.delegate = self //Adonde responde
        service.downloadPokemons()
    }

    func get20FisrtPokemons(pokemons: [Pokemon]) {
        pokemonArray = pokemons
        pokedexTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
    
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch section {
//        case 0:
//            return 5
//        default:
//            return 10
//        }Shift command 7
        return pokemonArray.count
    }
    
    func tableView(_ tableView: UITableView,cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell //Otro tipo de celda
        cell.fillData(pokemon: pokemonArray[indexPath.row])
        //cell.textLabel?.text=pokemonArray[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Seccion 1"
        default:
            return "Seccion 2"
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        pokemonIndex = indexPath.row
        return indexPath
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        let pokemonDetail = segue.destination as! DetalleViewController
        pokemonDetail.pokemon = pokemonArray[pokemonIndex]
        }
    
}
