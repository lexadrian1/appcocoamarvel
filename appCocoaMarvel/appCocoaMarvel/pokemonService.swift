//
//  pokemonService.swift
//  appCocoaMarvel
//
//  Created by Laboratorio FIS on 2/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

protocol PokemonServiceDelegate{
    func get20FisrtPokemons(pokemons:[Pokemon])
}

class pokemonService{
    var delegate:PokemonServiceDelegate?
    
    func downloadPokemons(){
        var pokemonArray:[Pokemon] = []
        let dpGR = DispatchGroup()
        for i in 1...20{
            dpGR.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject { (response: DataResponse<Pokemon>) in
                let pokemon = response.result.value
                pokemonArray.append(pokemon!)
                //print("leave queue \(i)")
                dpGR.leave()
        }
    }
        dpGR.notify(queue: .main){
            let sortedArray = pokemonArray.sorted(by: {$0.id! < $1.id!})//Ordenar Array
            self.delegate?.get20FisrtPokemons(pokemons: sortedArray)
        }
}
}
